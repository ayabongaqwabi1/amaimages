import React, { useState, useEffect } from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import InfiniteImages from "../components/InfiniteImages"
import AdSense from 'react-adsense';

const Gallery = () => {
  return (
    <Layout>
      <SEO title="Gallery" />
      <h1 className="is-size-3">Let's react!</h1>
      <p style={{ marginBottom: "5%" }}>
        Listed below are some of the best images used as twitter or facebook reactions.
        <br />
      </p>
      <p style={{color:"green"}}>
        You can download images by right clicking and clicking saimage as 
        <br />
        or you can click on it and the browser will open anther tab where you can right click and save image as.
      </p>
      <br/>
      <AdSense.Google
        client='ca-pub-8294995671791919'
        slot='7806394673'
        style={{ display: 'block' }}
        format='auto'
        responsive='true'
        layoutKey='-gw-1+2a-9x+5c'
      />
      <InfiniteImages />
    </Layout>
  )
}

export default Gallery
