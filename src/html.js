import React from "react"
import PropTypes from "prop-types"
import logo from "./images/logo.png"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta property="og:title" content="AmaPicture" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="http://www.amapicture.com" />
        <meta property="og:image" content={logo} />
        <meta property="og:image" content="https://i.ibb.co/mF3DBzn/logo.png" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {props.headComponents}
        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=UA-164692738-3"
        ></script>

        <meta
          name="propeller"
          content="947b9d6100a551da7d18fa07f569ca23"
        ></meta>
        <script
          src="https://propu.sh/pfe/current/tag.min.js?z=3329040"
          data-cfasync="false"
          async
        ></script>
        <script
          type="text/javascript"
          src="//graizoah.com/apu.php?zoneid=3329094"
          async
          data-cfasync="false"
        ></script>
        <script
          type="text/javascript"
          src="//graizoah.com/afu.php?zoneid=3329099"
          async
          data-cfasync="false"
        ></script>
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <noscript key="noscript" id="gatsby-noscript">
          This app works best with JavaScript enabled.
        </noscript>
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
