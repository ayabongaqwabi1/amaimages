import React, { useState, useEffect, useCallback } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import AdSense from "react-adsense"
import PropTypes from "prop-types"
import axios from "axios"
import InfiniteScroll from "react-infinite-scroll-component"
import "./gallery.scss"
import Gallery from "react-photo-gallery"

const download = url => {
  var element = document.createElement("a")
  var file = new Blob([url], { type: "image/*" })
  element.href = URL.createObjectURL(file)
  element.download = "image.jpg"
  element.click()
}

const SelectedImage = ({ photo, margin, direction, top, left }) => {
  return (
    <a
      href={photo.src}
      onClick={() => download(photo.src)}
      target="_blank"
      className="image-holder"
      style={{ margin, height: photo.height, width: photo.width }}
    >
      <img alt={photo.title} {...photo} />
      <span>Click image to download</span>
    </a>
  )
}

const ImageGallery = ({ images, loading, fetchImages }) => {
  // Create gallery here
  function shuffle(array) {
    array.sort(() => Math.random() - 0.5)
  }
  const photos = shuffle(images).map(i => ({
    src: i.url,
    width: i.width / 100,
    siblings: <span>I am a child</span>,
    height: i.height / 100,
  }))

  const imageRenderer = useCallback(({ index, left, top, key, photo }) => {
    return (
      <SelectedImage
        key={key}
        margin={"2px"}
        index={index}
        photo={photo}
        left={left}
        top={top}
      />
    )
  })
  return (
    <InfiniteScroll
      dataLength={images.length}
      next={() => fetchImages()}
      hasMore={true}
      loader={
        <p style={{ textAlign: "center", marginTop: "1%" }}>
          Images are coming 🤣 🤣 🤣
        </p>
      }
    >
      <div className="gallery" id="gallery">
        {!loading ? (
          <Gallery
            photos={photos}
            direction={"row"}
            renderImage={imageRenderer}
          />
        ) : (
          ""
        )}
      </div>
    </InfiniteScroll>
  )
}

const InfiniteImages = () => {
  // Hold state
  const [images, setImages] = useState([])
  const [loading, setLoading] = useState(true)

  // Fetch images on component mount
  useEffect(() => {
    fetchImages()
  }, [])

  // Fetch Images from functions
  const fetchImages = () => {
    axios("/.netlify/functions/fetch").then(res => {
      console.log(res.data.images.resources)
      setImages([...images, ...res.data.images.resources])
      setLoading(false)
    })
  }
  return (
    <ImageGallery images={images} loading={loading} fetchImages={fetchImages} />
  )
}

ImageGallery.propTypes = {
  images: PropTypes.array,
  loading: PropTypes.bool,
  fetchImages: PropTypes.func,
}

export default InfiniteImages
