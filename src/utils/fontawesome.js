import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowAltCircleDown } from '@fortawesome/free-solid-svg-icons'
library.add(faArrowAltCircleDown)
