import axios from "axios"
import config from "../../config"

exports.handler = function(event, context, callback) {
  // const apiRoot = "https://api.unsplash.com"
  const accessKey = process.env.ACCESS_KEY || config.apiKey
  const accessSecrete = process.env.ACCESS_SECRET || config.apiSecret
  // const doggoEndpoint = `${apiRoot}/photos/random?client_id=${accessKey}&count=${10}&collections='3816141,1154337,1254279'`
  console.log("getting cloudinary")
  axios.get(`https://${accessKey}:${accessSecrete}@api.cloudinary.com/v1_1/midas-touch-technologies/resources/image/?max_results=1000`)
          .then(res => {
            console.log("data")
            console.log(res.data.resources);
            callback(null, {
              statusCode: 200,
              body: JSON.stringify({
                images: res.data,
              }, null, 4),
            })
           })
           .catch(err => {
             console.log("hit an error")
             console.log(err)
           })
  // axios.get(doggoEndpoint).then(res => {
    // callback(null, {
    //   statusCode: 200,
    //   body: JSON.stringify({
    //     images: res.data,
    //   }),
    // })
  // })
}
